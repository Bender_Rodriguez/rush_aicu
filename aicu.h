/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aicu.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 13:52:16 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/20 17:52:05 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef AICU_H
#define AICU_H

#include "libft.h"
#include "colors.h"

typedef struct		s_ai
{
	int				*map;
	int				lines;
	int				error;
	int				index;
	int				matches;
}					t_ai;

void				initall(t_ai *all, char **av);
void				game(t_ai *all);
void				remove_match(t_ai *all, int picked);
void				open_error(char *file);
void				result(int who);
void				compute(t_ai *all);

#endif
