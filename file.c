/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 16:39:24 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/20 18:09:01 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aicu.h"

void				open_error(char *file)
{
	putcolor("Can't open file: ", BOLD_RED, 2, 0);
	putcolor(file, BOLD_RED, 2, 1);
	exit(0);
}

void				result(int who)
{
	if (who == 0)
		putcolor("Congratulations, you win !!!", BOLD_BROWN, 1, 1);
	else
		putcolor("Sorry, you lost, noob", BOLD_MAGENTA, 1, 1);
	exit (0);
}

void				remove_match(t_ai *all, int picked)
{
	if (all->map[all->index] >= picked)
		all->map[all->index] -= picked;
	if (all->map[all->index] == 0)
		all->index++;
}

void				compute(t_ai *all)
{
	if (all->map[all->index] % 4 == 0)
	{
		remove_match(all, 3);
		putcolor("Computer picked 3 matche(s)", MAGENTA, 1, 1);
		all->matches -= 3;
	}
	else
	{
		remove_match(all, 1);
		putcolor("Computer picked 1 matche(s)", MAGENTA, 1, 1);
		all->matches -= 1;
	}
}
