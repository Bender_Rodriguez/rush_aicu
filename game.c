/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 15:32:20 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/20 18:08:00 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aicu.h"

static void		print_board(t_ai *all)
{
	int			i;
	int			j;

	i = 0;
	while (i < all->lines)
	{
		j = 0;
		while (j < all->map[i])
		{
			ft_putstr("| ");
			j++;
		}
		if (j)
			ft_putchar('\n');
		i++;
	}
}

static int		prompt(int max)
{
	int			ok;
	char		*picked;
	int			nb;

	ok = 0;
	while (ok == 0)
	{
		ft_putstr("How many do you want to remove: ");
		if (get_next_line(0, &picked) != -1)
		{
			nb = ft_atoi(picked);
			if ((nb != 3 && nb != 2 && nb != 1 ))
				ft_putstr("please choose 1, 2 or 3\n");
			else if (nb > max)
				ft_putstr("Not enough matches on this row\n");
			else if (nb == 1 || nb == 2 || nb == 3)
				ok = 1;
		}
	}
	ft_putstr("You picked ");
	ft_putstr(picked);
	ft_putstr(" matche(s) / ");
	return (ft_atoi(picked));
}

void			game(t_ai *all)
{
	int			i;
	int			picked;

	i = 0;
	print_board(all);
	picked = prompt(all->map[all->index]);
	all->matches -= picked;
	ft_putchar(' ');
	ft_putendl(ft_itoa(all->matches));
	remove_match(all, picked);
	print_board(all);
	if (all->matches == 1)
		result(0);
	compute(all);
	if (all->matches == 1)
		result(1);

}
