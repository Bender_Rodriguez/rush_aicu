/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 13:57:32 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/20 17:08:25 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aicu.h"

static void		begin(t_ai *all, int ac, char **av)
{
	if (ac == 1)
		all = NULL;
	else
	{
		initall(all, av);
		if (all->error != 1)
		{
			ft_putstr("--- Start ---\n");
			while (all->matches > 1)
				game(all);
		}
	}

}

int				main(int ac, char **av)
{
	t_ai		all;

	all = (t_ai){NULL, 0, 0, 0, 0};
	if (ac == 1 || ac == 2)
		begin(&all, ac, av);
	else
		putcolor("ERROR", BOLD_RED, 2, 1);
	return (0);
}
