/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 14:07:42 by yaitalla          #+#    #+#             */
/*   Updated: 2015/12/20 17:03:39 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "aicu.h"



static int			iterstr(char *line)
{
	if (*line == '\0')
		return (1);
	return (ft_isdigit(*line) && iterstr(line + 1));
}

static int			check_line(char *line)
{
	int				i;

	i = ft_atoi(line);
	if ((i < 1 || i > 10000) || !iterstr(line))
		return (0);
	return (i);
}

void				initall(t_ai *all, char **av)
{
	int				i;
	int				fd;
	char			*line;
	int				nb;

	i = 0;
	all->map = (int *)malloc(sizeof(int) * 1024);
	fd = open(av[1], O_RDONLY);
	if (fd == -1)
		open_error(av[1]);
	while (get_next_line(fd, &line) > 0 && (*line))
	{
		if ((nb = check_line(line)) == 0)
		{
			all->error = 1;
			break ;
		}
		else
		{
			all->map[i] = nb;
			all->matches += nb;
		}
		i++;
	}
	all->lines = i;
	close(fd);
}
